package akcompany.session3;

import android.content.Context;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class InformationActivity extends AppCompatActivity {
    EditText FullName, Age, Email, BirthDate, Gender;

    Context mContext = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_information);
        Binding();

        FullName.setText(
                PreferenceManager.getDefaultSharedPreferences(mContext)
                        .getString("FullName", "")
        );
        Age.setText(
                PreferenceManager.getDefaultSharedPreferences(mContext)
                        .getString("Age", "")
        );
        Email.setText(
                PreferenceManager.getDefaultSharedPreferences(mContext)
                        .getString("Email", "")
        );
        BirthDate.setText(
                PreferenceManager.getDefaultSharedPreferences(mContext)
                        .getString("BirthDate", "")
        );
        Gender.setText(
                PreferenceManager.getDefaultSharedPreferences(mContext)
                        .getString("Gender", "")
        );


        findViewById(R.id.BtnClear).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Clear();
            }
        });
        findViewById(R.id.BtnSave).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PreferenceManager("Age", Age.getText().toString());
                PreferenceManager("Email", Email.getText().toString());
                PreferenceManager("BirthDate", BirthDate.getText().toString());
                PreferenceManager("Gender", Gender.getText().toString());
                PreferenceManager("FullName", FullName.getText().toString());
                ShowToast("Your information Saved");
                Clear();
            }
        });
    }

    void Binding() {
        FullName = findViewById(R.id.FullName);
        Age = findViewById(R.id.Age);
        Email = findViewById(R.id.Email);
        BirthDate = findViewById(R.id.BirthDate);
        Gender = findViewById(R.id.Gender);

    }

    void Clear() {

        FullName.setText("");

        Age.setText("");

        Email.setText("");

        BirthDate.setText("");

        Gender.setText("");

    }

    void ShowToast(String Text) {
        Toast.makeText(mContext, Text, Toast.LENGTH_SHORT).show();
    }

    void PreferenceManager(String Name, String Value) {
        PreferenceManager.getDefaultSharedPreferences(mContext).edit().putString(Name, Value).apply();
    }

}

